package ru.rencredit.jschool.kuzyushin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.dto.Result;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;

public interface ISessionEndpoint {

    @Nullable
    Session openSession(String login, String password) throws Exception;

    @NotNull
    Result closeSession(Session session);

    @NotNull
    Result closeAllUserSession(Session session);
}
