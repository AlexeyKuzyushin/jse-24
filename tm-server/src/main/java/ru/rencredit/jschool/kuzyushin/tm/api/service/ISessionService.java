package ru.rencredit.jschool.kuzyushin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import java.util.List;

public interface ISessionService extends IService<Session> {

    @Nullable
    Session open(@Nullable String login, @Nullable String password);

    @Nullable
    Session sign(@Nullable Session session);

    @Nullable
    User getUser(@Nullable Session session);

    @Nullable
    String getUserId(@Nullable Session session);

    @NotNull
    List<Session> getListSession(@Nullable Session session);

    boolean checkDataAccess(@Nullable String Login, @Nullable String password);

    boolean isValid(@Nullable Session session);

    void close(@Nullable Session session) throws Exception;

    void closeAll(@Nullable Session session);

    void validate (@Nullable Session session);

    void validate (@Nullable Session session, Role role);

    void signOutByLogin(@Nullable String login);

    void singOutByUserId(@Nullable String userId);
}
