package ru.rencredit.jschool.kuzyushin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;

public interface ISessionService {

    @Nullable
    Session getCurrentSession();

    void setCurrentSession(Session currentSession);

    void clearCurrentSession();
}
