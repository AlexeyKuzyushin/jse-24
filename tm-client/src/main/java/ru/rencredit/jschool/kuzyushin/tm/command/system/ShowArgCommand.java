package ru.rencredit.jschool.kuzyushin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

import java.util.List;

public final class ShowArgCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String description() {
        return "Show application arguments";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @NotNull final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
            for (@NotNull final AbstractCommand command: commands)
                if (command.arg() != null)
                    System.out.println(command.arg());
        }
        else System.out.println("[FAILED]");
    }
}
