package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Task;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class TaskViewByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-view-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by name";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        if (serviceLocator != null) {
            @Nullable final String name = TerminalUtil.nextLine();
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            @Nullable final Task task = serviceLocator.getTaskEndpoint().findTaskByName(session, name);
            if (task == null) return;
            System.out.println("ID: " + task.getId());
            System.out.println("NAME: " + task.getName());
            System.out.println("DESCRIPTION: " + task.getDescription());
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
